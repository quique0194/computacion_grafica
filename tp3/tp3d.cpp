#define GLUT_DISABLE_ATEXIT_HACK
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GL/glut.h>
using namespace std;

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1

#define ECHAP 27

void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);

//function called on each frame
GLvoid window_idle();

float x_translation = 0,
z_translation = -30,
x_rotation = 0,
y_rotation = 0,
x_mouse = 0,    // Guarda la ultima posicion del mouse en un drag continuo
y_mouse = 0,    // Guarda la ultima posicion del mouse en un drag continuo
delta = 0.3;

GLvoid callback_special(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
        z_translation += delta;
        glutPostRedisplay();            // et on demande le réaffichage.
        break;

    case GLUT_KEY_DOWN:
        z_translation -= delta;
        glutPostRedisplay();            // et on demande le réaffichage.
        break;

    case GLUT_KEY_LEFT:                 
        x_translation += delta;
        glutPostRedisplay();            // et on demande le réaffichage.
        break;

    case GLUT_KEY_RIGHT:
        x_translation -= delta;
        glutPostRedisplay();            // et on demande le réaffichage.
        break;
    } 
}

GLvoid callback_mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
    {
        x_mouse = x;
        y_mouse = y;
    }
}

float x_anterior=0, y_anterior=0;
GLvoid callback_motion(int x, int y)
{
    printf("%d  %d\n", x, y);
    x_rotation += 0.5 * (x-x_mouse);
    y_rotation += 0.5 * (y-y_mouse);
    x_mouse = x;
    y_mouse = y; 
    glutPostRedisplay();                        
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("TP 2 : Transformaciones");


    initGL();
    init_scene();

    glutDisplayFunc(&window_display);

    glutReshapeFunc(&window_reshape);

    glutKeyboardFunc(&window_key);

    //function called on each frame
    glutIdleFunc(&window_idle);

    // capture mouse and keyboard
    glutSpecialFunc(&callback_special);
    glutMouseFunc(&callback_mouse);
    glutMotionFunc(&callback_motion);

    glutMainLoop();

    return 1;
}



GLvoid initGL()
{
    GLfloat position[] = { 0.0f, 5.0f, 10.0f, 0.0 };

    //enable light : try without it
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glEnable(GL_LIGHTING);
    //light 0 "on": try without it
    glEnable(GL_LIGHT0);

    //shading model : try GL_FLAT
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);

    //enable material : try without it
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(RED, GREEN, BLUE, ALPHA);
}

void gizmo3d() {
    glBegin(GL_LINES);
    glColor3d(255,0,0);
    glVertex3d(0, 0, 0);
    glVertex3d(10, 0, 0);

    glColor3d(0, 255, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 10, 0);

    glColor3d(0, 0, 255);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 0, 10);
    glEnd();
}

GLvoid window_display()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);
    gluPerspective(60, 4.0/3.0, 0.1, 100);


    /*dibujar aqui*/
    glTranslatef(x_translation, 0, z_translation);
    glRotatef(x_rotation, 0, 1, 0);
    glRotatef(y_rotation, 1, 0, 0);
    gizmo3d();

    glutSwapBuffers();

    glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);
    gluPerspective(60, 4.0/3.0, 0.1, 100);

    glMatrixMode(GL_MODELVIEW);
}



void init_scene()
{

}

GLvoid window_key(unsigned char key, int x, int y)
{
    switch (key) {
    case ECHAP:
        exit(1);
        break;

    default:
        printf("La touche %d non active.\n", key);
        break;
    }
}


//function called on each frame
GLvoid window_idle()
{


    glutPostRedisplay();
}
