#ifndef VECTOR3F_H
#define VECTOR3F_H

#include <cmath>
#include <GL/glut.h>

class Vector3f {
    public:
        Vector3f(GLfloat x=0, GLfloat y=0, GLfloat z=0) {
            set_data(x, y, z);
        }
        Vector3f operator-(const Vector3f& v) const {
            Vector3f ret;
            for (int i = 0; i < 3; ++i) {
                ret.data[i] = data[i] - v.data[i];
            }
            return ret;
        }
        Vector3f operator+(const Vector3f& v) const {
            Vector3f ret;
            for (int i = 0; i < 3; ++i) {
                ret.data[i] = data[i] + v.data[i];
            }
            return ret;
        }
        Vector3f cruz(const Vector3f& v) const {
            Vector3f ret;
            ret.data[0] = data[1]*v.data[2] - data[2]*v.data[1];
            ret.data[1] = data[2]*v.data[0] - data[0]*v.data[2];
            ret.data[2] = data[0]*v.data[1] - data[1]*v.data[0];
            return ret;
        }
        GLfloat length() const {
            return sqrt(data[0]*data[0] + data[1]*data[1] + data[2]*data[2]);
        }
        Vector3f normalize() const {
            Vector3f ret;
            GLfloat l = length();
            for (int i = 0; i < 3; ++i) {
                ret.data[i] = data[i]/l;
            }
            return ret;
        }
        void set_data(GLfloat x, GLfloat y, GLfloat z) {
            data[0] = x;
            data[1] = y;
            data[2] = z;
        }
        void print() {
            for (int i = 0; i < 3; ++i) {
                cout << data[i] << " ";
            }
            cout << endl;
        }
    private:
        GLfloat data[3];
};

#endif