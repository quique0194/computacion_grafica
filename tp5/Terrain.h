#ifndef TERRAIN_H
#define TERRAIN_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
using namespace std;

#include <GL/glut.h>

#include "./TextureManager.h"
#include "./Vector3f.h"


struct TerrainPoint {
    GLfloat s, t;           // Coordenadas de textura
    Vector3f normal;
    Vector3f position;
};

class Terrain {
    public:
        Terrain(const char* txt, const char* img) {
            load_txt(txt);
            compute_index_list();
            calculate_triangle_normals();
            calculate_adjacencies();
            calculate_vertex_normals();
            texture = TextureManager::Inst()->LoadTexture(img, GL_BGR_EXT, GL_RGB);
            printf("%d\n", texture);
        }
        void display() {
            glBindTexture(GL_TEXTURE_2D, texture);

            glInterleavedArrays(GL_T2F_N3F_V3F, sizeof(TerrainPoint), (void*)vertex_list);
            glDrawElements(GL_TRIANGLES, number_of_triangles*3, GL_UNSIGNED_INT, (void*)index_list);
        }
        ~Terrain() {
            delete[] vertex_list;
            delete[] index_list;
            delete[] normal_list;
        }
    private:
        double dx;  // delta x
        double dz;  // delta z
        double ds;  // delta texture coord s
        double dt;  // delta texture coord t
        int width;
        int height;
        GLint texture;
        int number_of_triangles;
        int number_of_vertexes;
        TerrainPoint* vertex_list;
        GLuint* index_list;
        Vector3f* normal_list;  // 1 normal per triangle
        vector<vector<int> > adjacencies;   // adjacencies[vertex_idx] =
                                            // triangle_idx_list

        int get_idx(int row, int col) {
            return row*width + col;
        }

        void load_txt(const char* txt) {
            string ignore;
            ifstream f(txt);
            for (int _ = 0; _ < 7; ++_) {
                f >> ignore;
            }
            f >> dx >> dz >> width >> height;
            for (int _ = 0; _ < 2; ++_) {
                f >> ignore;
            }

            ds = 1.0/(width-1);
            dt = 1.0/(height-1);

            number_of_vertexes = width*height;
            vertex_list = new TerrainPoint[number_of_vertexes];

            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    int vertex_idx = get_idx(i, j);
                    GLfloat y; f >> y;
                    vertex_list[vertex_idx].position.set_data(j*dx, y, i*dz);

                    vertex_list[vertex_idx].s = j*ds;
                    vertex_list[vertex_idx].t = i*dt;
                }
            }
            f.close();
        }

        void compute_index_list() {
            number_of_triangles = (width-1)*(height-1)*2;
            index_list = new GLuint[number_of_triangles*3];
            cout << "numer number_of_triangles: " << number_of_triangles << endl;

            int index_list_idx = 0;
            for (int i = 0; i < height-1; ++i) {
                for (int j = 0; j < width-1; ++j) {
                    index_list[index_list_idx++] = get_idx(i, j);
                    index_list[index_list_idx++] = get_idx(i+1, j);
                    index_list[index_list_idx++] = get_idx(i, j+1);

                    index_list[index_list_idx++] = get_idx(i, j+1);
                    index_list[index_list_idx++] = get_idx(i+1, j);
                    index_list[index_list_idx++] = get_idx(i+1, j+1);
                }
            }
        }

        void calculate_triangle_normals() {
            normal_list = new Vector3f[number_of_triangles];
            for (int i = 0; i < number_of_triangles; ++i) {
                int v1_idx = index_list[i*3],
                v2_idx = index_list[i*3 + 1],
                v3_idx = index_list[i*3 + 2];

                Vector3f v1 = vertex_list[v1_idx].position,
                v2 = vertex_list[v2_idx].position,
                v3 = vertex_list[v3_idx].position,

                a = v2 - v1,
                b = v3 - v1,
                normal = b.cruz(a).normalize();

                normal_list[i] = normal;
            }
        }

        void calculate_adjacencies() {
            adjacencies.assign(number_of_vertexes, vector<int>());
            for (int i = 0; i < number_of_triangles; ++i) {
                for (int j = 0; j < 3; ++j) {
                    int vertex_idx = index_list[i*3+j];
                    adjacencies[vertex_idx].push_back(i);
                }
            }
        }

        void calculate_vertex_normals() {
            for (int i = 0; i < number_of_triangles*3; ++i) {
                int vertex_idx = index_list[i];
                vector<int>* triangles = &adjacencies[vertex_idx];
                Vector3f avg;
                for (int j = 0; j < triangles->size(); ++j) {
                    int triangle_idx = (*triangles)[j];
                    avg = avg + normal_list[triangle_idx];
                }
                vertex_list[vertex_idx].normal = avg.normalize();
            }
        }
};

#endif
