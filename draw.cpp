#define GLUT_DISABLE_ATEXIT_HACK	
#include <math.h>
#include <iostream>
#include <vector>
#include <GL/glut.h>

#define KEY_ESC 27
struct Point2D
{
	float x;
	float y;
};

std::vector<Point2D> points;

//
//funcion llamada a cada imagen
void glPaint(void) {

	//El fondo de la escena al color initial
	glClear(GL_COLOR_BUFFER_BIT);
	glOrtho(0.0f, 600.0f, 0.0f, 600.0f, -1.0f, 1.0f);
	glLoadIdentity();
	

	glBegin(GL_POINTS);
	for (std::vector<Point2D>::iterator it = points.begin(); it != points.end(); ++it)
	{
		
		glVertex2d(it->x, it->y);
	}
	glEnd();
		


	//doble buffer, mantener esta instruccion al fin de la funcion
	glutSwapBuffers();
}

void idleFunc()
{
	glutPostRedisplay();
}

//
//inicializacion de OpenGL
//
void init_GL(void) {
	//Color del fondo de la escena
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //(R, G, B, transparencia) en este caso un fondo negro

	//modo projeccion 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

//en el caso que la ventana cambie de tamaño
GLvoid window_redraw(GLsizei width, GLsizei height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0.0f, 600.0f, 0.0f, 600.0f, -1.0f, 1.0f);
	// todas la informaciones previas se aplican al la matrice del ModelView
	glMatrixMode(GL_MODELVIEW);
}


GLvoid window_key(unsigned char key, int x, int y) {
	switch (key) {
	case KEY_ESC:
		exit(0);
		break;

	default:
		break;
	}

}

GLvoid callback_mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
	{
		
	}
}


GLvoid callback_motion(int x, int y)
{
	Point2D p;
	p.x = x;
	p.y = 600 - y;

	points.push_back(p);
	glutPostRedisplay();
}

//
//el programa principal
//
int main(int argc, char** argv) {

	//Inicializacion de la GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 600); //tamaño de la ventana
	glutInitWindowPosition(100, 100); //posicion de la ventana
	glutCreateWindow("TP1 OpenGL : hello_world_OpenGL"); //titulo de la ventana

	init_GL(); //fucnion de inicializacion de OpenGL

	glutDisplayFunc(glPaint);
	glutReshapeFunc(&window_redraw);
	// Callback del teclado
	glutKeyboardFunc(&window_key);
	glutMouseFunc(&callback_mouse);
	glutMotionFunc(&callback_motion);
	glutIdleFunc(&idleFunc);

	glutMainLoop(); //bucle de rendering

	return 0;
}