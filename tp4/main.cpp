#define GLUT_DISABLE_ATEXIT_HACK    
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GL/glut.h>
using namespace std;

#include "./TextureManager.h"
#include "./glUtils.h"
 using namespace glUtils;

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1

#define ECHAP 27

void init_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);


//variables para el gizmo
float delta_x = 0.0; 
float delta_y = 0.0;
float mouse_x, mouse_y;
float var_x = 0.0;
float var_z = -30.0;
float step = 0; //0.0 Posicion inicial. 1.0 Traslacion. 2.0 Primera Rotacion(en y). 3.0 Segunda Rotacion (en x) 4.0 Ultima Rotacion (en z)

// textures
GLint grass_texture;
GLint wall_texture;
GLint roof_texture;

///////////////////////////////////////////////////////////////////////////////
// (1)
///////////////////////////////////////////////////////////////////////////////
GLvoid callback_special(int key, int x, int y) {
    switch (key) {
    case GLUT_KEY_UP:
        var_z += 0.5;
        glutPostRedisplay();
        break;

    case GLUT_KEY_DOWN:
        var_z -= 0.5;
        glutPostRedisplay();
        break;

    case GLUT_KEY_LEFT:
        var_x += 0.5;
        glutPostRedisplay();
        break;

    case GLUT_KEY_RIGHT:
        var_x -= 0.5;
        glutPostRedisplay();
        break;

    case GLUT_KEY_PAGE_UP:
        step++;
        glutPostRedisplay();
        break;
    case GLUT_KEY_PAGE_DOWN:
        step--;
        glutPostRedisplay();
        break;
    }
}



///////////////////////////////////////////////////////////////////////////////
// (2)
///////////////////////////////////////////////////////////////////////////////
GLvoid callback_mouse(int button, int state, int x, int y) {
    if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON) {
        mouse_x = x;
        mouse_y = y;
    }
}

///////////////////////////////////////////////////////////////////////////////
// (3)
///////////////////////////////////////////////////////////////////////////////
GLvoid callback_motion(int x, int y) {
    delta_x += x - mouse_x;
    delta_y += y - mouse_y;
    mouse_x = x;
    mouse_y = y;
    glutPostRedisplay();
}

void init_light() {
    GLfloat light_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_position[] = { -20.0, 1.0, 1.0, 1.0 };
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}

void move_light(const GLfloat* light_position) {
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}

GLvoid initGL() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glClearColor(RED, GREEN, BLUE, ALPHA);
    init_light();
}


void Gizmo3D() {
    glBegin(GL_LINES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(3.0f, 0.0f, 0.0f);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 3.0f, 0.0f);
    glEnd();


    glBegin(GL_LINES);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 3.0f);
    glEnd();

    return;
}

void floor(double width, double height) {
    set_material(0.3, 0.3, 0.3);
    plane_with_texture(width, height, 'y', grass_texture, width/2, height/2);
}

void house(double x, double y, double z) {
    set_material(0.3, 0.3, 0.3);
    // walls
    double wall_height = 2*y/3;
    glPushMatrix();
        // front
        plane_with_texture(x, wall_height, 'z', wall_texture, x/2, wall_height/2, -1);
        // left
        plane_with_texture(z, wall_height, 'x', wall_texture, x/2, wall_height/2, -1);
        // back
        glTranslatef(x, 0, z);
        plane_with_texture(-x, wall_height, 'z', wall_texture, x/2, wall_height/2);
        // right
        plane_with_texture(-z, wall_height, 'x', wall_texture, x/2, wall_height/2);
    glPopMatrix();
    // triangles
    double roof_height = y/3;
    glPushMatrix();
        // left
        glTranslatef(0, wall_height, 0);
        triangle_with_texture(z, roof_height, 'x', wall_texture, z/2, roof_height/2, -1);
        // right
        glTranslatef(x, 0 , 0);
        triangle_with_texture(z, roof_height, 'x', wall_texture, z/2, roof_height/2);
    glPopMatrix();
    // roof
    double hypotenuse = sqrt((z/2)*(z/2) + roof_height*roof_height);
    double angle = atan(2*roof_height/z);
    angle = angle*180/M_PI;     // radians to sexagesimal
    glPushMatrix();
        // front
        glTranslatef(0, wall_height, 0);
        glRotatef(90-angle, 1, 0, 0);
        plane_with_texture(x, hypotenuse, 'z', roof_texture, x/2, hypotenuse/2, -1);
    glPopMatrix();
    glPushMatrix();
        // back
        glTranslatef(0, wall_height, z);
        glRotatef(-90+angle, 1, 0, 0);
        plane_with_texture(x, hypotenuse, 'z', roof_texture, x/2, hypotenuse/2);
    glPopMatrix();
}

void tree(double height, double radius) {
    set_material(0.3, 0, 0);
    glPushMatrix();
        glRotatef(-90, 1, 0, 0);
        GLUquadricObj *obj = gluNewQuadric();
        gluCylinder(obj, radius/4, radius/4, height-radius, radius*10, 1);
    glPopMatrix();
    set_material(0, 0.3, 0.3);
    glPushMatrix();
        glTranslatef(0, height-radius, 0);
        glutSolidSphere(radius, radius*10, radius*10);
    glPopMatrix();
}

GLfloat light_position[] = { 0, 10.0, 0.0, 1.0 };
double delta = 0.1;

GLvoid window_display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();


    gluPerspective(45.0f, 1.0f, 0.01f, 100.0f);

    glTranslatef(var_x, 0.0, var_z);
    glRotatef(delta_x, 0.0, 1.0, 0.0);
    glRotatef(delta_y, 1.0, 0.0, 0.0);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    Gizmo3D();

    if (light_position[0] < -20 || light_position[0] > 20) {
        delta = -delta;
    }
    light_position[0] += delta;
    move_light(light_position);
    glPushMatrix();
        set_material(0.6, 0.6, 0);
        glTranslatef(light_position[0], light_position[1], light_position[2]);
        glutSolidSphere(1, 10, 10);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-20, 0, -20);
        floor(40, 40);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-15, 0, -15);
        house(8, 8, 6);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0, 0, -10);
        tree(8, 2);
    glPopMatrix();

    glutSwapBuffers();

    glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height) {
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
}



void init_scene() {
}

GLvoid window_key(unsigned char key, int x, int y) {
    switch (key) {
    case ECHAP:
        exit(1);
        break;
    default:
        printf("La touche %d non active.\n", key);
        break;
    }
}


// function called on each frame
GLvoid window_idle() {
    glutPostRedisplay();
}

void load_textures() {
    grass_texture = TextureManager::Inst()->LoadTexture("img/grass.jpg",GL_BGR_EXT, GL_RGB);
    wall_texture = TextureManager::Inst()->LoadTexture("img/wall.jpg",GL_BGR_EXT, GL_RGB);
    roof_texture = TextureManager::Inst()->LoadTexture("img/roof.png",GL_BGR_EXT, GL_RGB);
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("TP 3 : Transformations 3D Part 2");


    initGL();
    init_scene();

    glutDisplayFunc(&window_display);
    glutReshapeFunc(&window_reshape);

    glutMouseFunc(&callback_mouse);
    glutMotionFunc(&callback_motion);

    glutKeyboardFunc(&window_key);
    // glutKeyboardUpFunc(&window_key_up); //key release events
    glutSpecialFunc(&callback_special);
    // glutSpecialUpFunc(&callback_special_up); //key release events

    // function called on each frame
    glutIdleFunc(&window_idle);

    load_textures();

    glutMainLoop();

    return 1;
}

