#ifndef GLUTILS_H
#define GLUTILS_H

namespace glUtils {
    void plane(double width, double height, char normal, int as, double sa, double asd) {
        /*
            Draws a plane with bottom-left corner in (0,0)

            normal: axis which is the plane's normal
                'x': plane is parallel to yz
                'y': plane is parallel to xz
                'z': plane is parallel to xy
        */
        if (normal == 'z') {
            glBegin(GL_QUADS);
                glVertex3f(0, 0, 0);
                glVertex3f(width, 0, 0);
                glVertex3f(width, height, 0);
                glVertex3f(0, height, 0);
            glEnd();
        } else if (normal == 'y') {
            glBegin(GL_QUADS);
                glVertex3f(0, 0, 0);
                glVertex3f(width, 0, 0);
                glVertex3f(width, 0, height);
                glVertex3f(0, 0, height);
            glEnd();
        } else if (normal == 'x') {
            glBegin(GL_QUADS);
                glVertex3f(0, 0, 0);
                glVertex3f(0, 0, width);
                glVertex3f(0, height, width);
                glVertex3f(0, height, 0);
            glEnd();
        }
    }

    void plane_with_texture(double width, double height,
                            char normal,
                            int texture,
                            double tex_x=1, double tex_y=1,
                            double normal_size=1) {
        /*
            Same as plane(), but put a texture over the plane

            texture: GLint handle of texture
            tex_x: Texture repeats tex_x times horizontally 
            tex_y: Texture repeats tex_y times vertically
        */

        glColor3f(1, 1, 1);
        glBindTexture(GL_TEXTURE_2D, texture);

        if (normal == 'z') {
            glBegin(GL_QUADS);
                glNormal3f(0, 0, normal_size);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);

                glTexCoord2f(tex_x, 0);
                glVertex3f(width, 0, 0);

                glTexCoord2f(tex_x, tex_y);
                glVertex3f(width, height, 0);

                glTexCoord2f(0, tex_y);
                glVertex3f(0, height, 0);
            glEnd();
        } else if (normal == 'y') {
            glBegin(GL_QUADS);
                glNormal3f(0, normal_size, 0);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);

                glTexCoord2f(tex_x, 0);
                glVertex3f(width, 0, 0);

                glTexCoord2f(tex_x, tex_y);
                glVertex3f(width, 0, height);

                glTexCoord2f(0, tex_y);
                glVertex3f(0, 0, height);
            glEnd();
        } else if (normal == 'x') {
            glBegin(GL_QUADS);
                glNormal3f(normal_size, 0, 0);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);
                
                glTexCoord2f(tex_x, 0);
                glVertex3f(0, 0, width);

                glTexCoord2f(tex_x, tex_y);
                glVertex3f(0, height, width);
                
                glTexCoord2f(0, tex_y);
                glVertex3f(0, height, 0);
            glEnd();
        }
    }

    void triangle_with_texture(double width, double height,
                               char normal,
                               int texture,
                               double tex_x=1, double tex_y=1,
                               double normal_size=1) {
        /*
            Same as plane(), but put a texture over the plane

            texture: GLint handle of texture
            tex_x: Texture repeats tex_x times horizontally 
            tex_y: Texture repeats tex_y times vertically
        */

        glColor3f(1,1,1);
        glBindTexture(GL_TEXTURE_2D, texture);

        if (normal == 'z') {
            glBegin(GL_POLYGON);
                glNormal3f(0, 0, normal_size);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);
                
                glTexCoord2f(tex_x, 0);
                glVertex3f(width, 0, 0);

                glTexCoord2f(tex_x/2, tex_y);
                glVertex3f(width/2, height, 0);
            glEnd();
        } else if (normal == 'y') {
            glBegin(GL_POLYGON);
                glNormal3f(0, normal_size, 0);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);
                
                glTexCoord2f(tex_x, 0);
                glVertex3f(width, 0, 0);

                glTexCoord2f(tex_x/2, tex_y);
                glVertex3f(width/2, 0, height);
            glEnd();
        } else if (normal == 'x') {
            glBegin(GL_POLYGON);
                glNormal3f(normal_size, 0, 0);

                glTexCoord2f(0, 0);
                glVertex3f(0, 0, 0);
                
                glTexCoord2f(tex_x, 0);
                glVertex3f(0, 0, width);

                glTexCoord2f(tex_x/2, tex_y);
                glVertex3f(0, height, width/2);
            glEnd();
        }
    }

    void set_material(double r, double g, double b, double a = 1.0,
                      int face = GL_FRONT) {
        GLfloat mat_ambient[4]  = {r, g, b, a};
        GLfloat mat_diffuse[4]  = {r, g, b, a};
        GLfloat mat_specular[4] = {0.2f, 0.2f, 0.2f, 1.0f};
        GLfloat mat_shininess[] = { 5.0F };
        glMaterialfv(face, GL_AMBIENT, mat_ambient);
        glMaterialfv(face, GL_DIFFUSE, mat_diffuse);
        glMaterialfv(face, GL_SPECULAR, mat_specular);
        glMaterialfv(face, GL_SHININESS, mat_shininess);
    }
}

#endif