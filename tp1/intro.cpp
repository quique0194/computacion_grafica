#define GLUT_DISABLE_ATEXIT_HACK    
#include <cstdlib>
#include <cmath>
#include <GL/glut.h>

#define KEY_ESC 27

//dibuja un simple gizmo
void displayGizmo()
{
    glBegin(GL_LINES);
    glColor3d(255,0,0);
    glVertex2d(0, 0);
    glVertex2d(1, 0);
    glColor3d(0, 255, 0);
    glVertex2d(0, 0);
    glVertex2d(0, 1);
    glEnd();
}

void displaySquare(int center_x, int center_y, int edge){
    glBegin(GL_LINE_LOOP);
    glVertex2d(center_x + edge/2, center_y + edge/2);
    glVertex2d(center_x + edge/2, center_y - edge/2);
    glVertex2d(center_x - edge/2, center_y - edge/2);
    glVertex2d(center_x - edge/2, center_y + edge/2);
    glEnd();
}

void displayCircle(float center_x, float center_y, float radius){
    int points = radius*3;
    double step = 2*M_PI/points;

    glBegin(GL_LINE_LOOP);
    for(double i=0; i<2*M_PI; i+=step){
        glVertex2f(center_x + radius*cos(i), center_y + radius*sin(i));
    }
    glEnd();
}

// factor should be in range [0..1>
void displayRinnegan(float center_x, float center_y, float radius, int circles, float factor){
    float scale = 1;
    while(circles--){
        displayCircle(center_x, center_y, radius);
        center_x = center_x - radius + radius*factor;
        radius *= factor;
    }
}

void displaySnake(float center_x, float center_y, float radius, int circles, float factor, float phi=0){
    while(circles--){
        displayCircle(center_x, center_y, radius);
        center_x += (radius + radius*factor) * cos(phi);
        center_y += (radius + radius*factor) * sin(phi);
        radius*=factor;
    }
}

//
//funcion llamada a cada imagen
void glPaint(void) {

    //El fondo de la escena al color initial
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
    
    //dibuja el gizmo
    displayGizmo();

    glColor3d(255,255,255);
    // Exercise 1
    displaySquare(-30,30,20);
    // Exercise 2
    displayCircle(0,30,10);
    // Exercise 3
    displayRinnegan(30, 30, 10, 5, 0.8);
    // Exercise 4
    displaySnake(-30, 0, 10, 5, 0.8);
    // Exercise 5
    displaySnake(-30, -30, 10, 5, 0.8, 0.3);


    //doble buffer, mantener esta instruccion al fin de la funcion
    glutSwapBuffers();
}

//
//inicializacion de OpenGL
//
void init_GL(void) {
    //Color del fondo de la escena
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //(R, G, B, transparencia) en este caso un fondo negro

    //modo projeccion 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
}

//en el caso que la ventana cambie de tama�o
GLvoid window_redraw(GLsizei width, GLsizei height) {
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    glOrtho(-50.0f,  50.0f,-50.0f, 50.0f, -1.0f, 1.0f); 
    // todas la informaciones previas se aplican al la matrice del ModelView
    glMatrixMode(GL_MODELVIEW);
}

GLvoid window_key(unsigned char key, int x, int y) {
    switch (key) {
    case KEY_ESC:
        exit(0);
        break;

    default:
        break;
    }

}
//
//el programa principal
//
int main(int argc, char** argv) {

    //Inicializacion de la GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(600, 600); //tama�o de la ventana
    glutInitWindowPosition(100, 100); //posicion de la ventana
    glutCreateWindow("TP1 OpenGL : hello_world_OpenGL"); //titulo de la ventana

    init_GL(); //fucnion de inicializacion de OpenGL

    glutDisplayFunc(glPaint); 
    glutReshapeFunc(&window_redraw);
    // Callback del teclado
    glutKeyboardFunc(&window_key);

    glutMainLoop(); //bucle de rendering

    return 0;
}
